terraform {
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
    }
  }
}

provider "openstack" {
  tenant_name = var.project
  region = var.region
}

resource "openstack_compute_instance_v2" "os_master_instance" {
  name = "${var.instance_name}_master"
  count = var.instance_count >= 1 ? 1 : 0
  image_id = local.image_uuid
  flavor_name = var.flavor_master
  key_pair = var.keypair
  security_groups = local.security_groups
  power_state = var.power_state
  user_data = var.user_data

  network {
        name = "${var.network}"
  }

  block_device {
    uuid = local.image_uuid
    source_type = var.root_storage_source
    destination_type = var.root_storage_type
    boot_index = 0
    delete_on_termination = var.root_storage_delete_on_termination
    volume_size = local.volume_size
  }

  lifecycle {
    precondition {
      condition = var.image != "" || var.image_name != ""
      error_message = "ERROR: template input image or image_name must be set"
    }
    ignore_changes = [
      image_id
    ]
  }
}

resource "openstack_compute_instance_v2" "os_worker_instance" {
  name = "${var.instance_name}_worker${count.index}"
  count = var.instance_count >= 2 ? var.instance_count - 1 : 0
  image_id = local.image_uuid
  flavor_name = var.flavor
  key_pair = var.keypair
  security_groups = local.security_groups
  power_state = var.power_state
  user_data = var.user_data

  network {
    name = "${var.network}"
  }

  block_device {
    uuid = local.image_uuid
    source_type = var.root_storage_source
    destination_type = var.root_storage_type
    boot_index = 0
    delete_on_termination = var.root_storage_delete_on_termination
    volume_size = local.volume_size
  }

  lifecycle {
    precondition {
      condition = var.image != "" || var.image_name != ""
      error_message = "ERROR: template input image or image_name must be set"
    }
    ignore_changes = [
      image_id
    ]
  }

  depends_on = [openstack_compute_instance_v2.os_master_instance[0]]
}

data "openstack_networking_network_v2" "ext_network" {
  # make the assumption that there is only 1 external network per region, this will fail if otherwise
  region = var.region
  external = true
}

resource "openstack_networking_floatingip_v2" "os_master_floatingip" {
  count = var.power_state == "active" && var.jupyterhub_floating_ip == "" ? 1 : 0
  pool = data.openstack_networking_network_v2.ext_network.name
  description = "floating ip for ${var.instance_name}_master"
  lifecycle {
    ignore_changes = [fixed_ip, port_id]
  }

}

resource "openstack_networking_floatingip_v2" "os_worker_floatingips" {
  count = var.power_state == "active" && var.instance_count >= 2 ? "${var.instance_count - 1}" : 0
  pool = data.openstack_networking_network_v2.ext_network.name
  description = "floating ip for ${var.instance_name}_worker${count.index} of ${var.instance_count - 1}"
  lifecycle {
    ignore_changes = [fixed_ip, port_id]
  }
}

# EJS - we need to incorporate a wait before associating floating ips since js2 neutron might need time to "think"
# We should later evaluate if this is just an IU issue or this is an issue across all clouds
# due to constraints of depends_on meta variable, I can only use the first element -- no template syntax, calculations, etc are allowed :(
resource "time_sleep" "master_fip_associate_timewait" {
  count = var.power_state == "active" ? 1 : 0
  depends_on = [openstack_compute_instance_v2.os_master_instance[0]]
  create_duration = var.fip_associate_timewait
}

resource "openstack_compute_floatingip_associate_v2" "os_master_floatingip_associate" {
  count = var.power_state == "active" ? 1 : 0
  floating_ip = var.jupyterhub_floating_ip == "" ? openstack_networking_floatingip_v2.os_master_floatingip.0.address : var.jupyterhub_floating_ip
  instance_id = openstack_compute_instance_v2.os_master_instance.0.id
  depends_on = [time_sleep.master_fip_associate_timewait[0]]
}

# EJS - we need to incorporate a wait before associating floating ips since js2 neutron might need time to "think"
# We should later evaluate if this is just an IU issue or this is an issue across all clouds
# due to constraints of depends_on meta variable, I can only use the first element -- no template syntax, calculations, etc are allowed :(
resource "time_sleep" "os_worker_fip_associate_timewait" {
  count = var.power_state == "active" && var.instance_count >= 2 ? 1 : 0
  depends_on = [openstack_compute_instance_v2.os_worker_instance[0], openstack_networking_floatingip_v2.os_worker_floatingips[0]]
  create_duration = var.fip_associate_timewait
}

resource "openstack_compute_floatingip_associate_v2" "os_worker_floatingips_associate" {
  count = var.power_state == "active" && var.instance_count >= 2 ? "${var.instance_count - 1}" : 0
  floating_ip = openstack_networking_floatingip_v2.os_worker_floatingips[count.index].address
  instance_id = openstack_compute_instance_v2.os_worker_instance[count.index].id
  depends_on = [time_sleep.os_worker_fip_associate_timewait[0]]
}

resource "openstack_sharedfilesystem_share_v2" "share_01" {
  count = var.jh_storage_size <= 0 ? 0 : 1
  name             = "${local.share_name_prefix}-share"
  description      = "jupyterhub share"
  share_proto      = "CEPHFS"
  size             = var.jh_storage_size

  # lifecycle {
  #   ignore_changes = [export_locations]
  # }

}
resource "openstack_sharedfilesystem_share_access_v2" "share_01_access" {
  count = var.jh_storage_size <= 0 ? 0 : 1
  share_id     = "${openstack_sharedfilesystem_share_v2.share_01.0.id}"
  access_type  = "cephx"
  access_to    = "${local.share_name_prefix}-share-access"
  access_level = "rw"
}

data "openstack_images_image_v2" "instance_image" {
  count = var.image_name == "" ? 0 : 1
  name = var.image_name
  most_recent = true
}

locals {
  share_name_prefix = lower(replace(replace(replace(var.instance_name,".","-")," ","-"),"_","-"))
  share_path_splitted = try(split(":", openstack_sharedfilesystem_share_v2.share_01.0.export_locations[0].path),[])
  share_ceph_root_path = try(element(local.share_path_splitted,length(local.share_path_splitted)-1),"")

  security_groups = try (
    split(",", var.security_groups),
    tolist(var.security_groups),
    ["default", "cacao-default"]
  )

  jupyterhub_allowed_users = try (
    split(",", var.jupyterhub_allowed_users),
    tolist(var.jupyterhub_allowed_users),
    []
  )

  jupyterhub_admins = try (
    split(",", var.jupyterhub_admins),
    tolist(var.jupyterhub_admins),
    []
  )

  # this will work with 'user1' and 'user1@example.com'
  split_username = split("@", var.username)
  system_user = local.split_username[0]

  # determine external hostname or ip
  jupyterhub_external_hostname = var.power_state != "active" ? "${var.instance_name}-inactive" : (var.jupyterhub_hostname == "" ? openstack_compute_floatingip_associate_v2.os_master_floatingip_associate.0.floating_ip : var.jupyterhub_hostname)
  jupyterhub_external_protocol = var.jupyterhub_oauth2_callback_https_enable ? "https://" : "http://"

  # needed for volumes
  image_uuid = var.image_name == "" ? var.image : data.openstack_images_image_v2.instance_image.0.id
  volume_size = var.root_storage_size > 0 ? var.root_storage_size : null
}
