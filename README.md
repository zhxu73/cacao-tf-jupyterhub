# cacao-tf-jupyterhub

This repo is a [Cacao](https://gitlab.com/cyverse/cacao)-compliant terraform module to deploy a [Zero to Jupyterhub](https://zero-to-jupyterhub.readthedocs.io/en/latest/), equiped with kubernetes (k3s). This currently has been tested on [Jetstream 1](https://www.jetstream-cloud.org) and will be tested on Jetstream 2, once it is available.

## Prerequisites

* Although not a strong requirement, you'll need to pass in the network id or name. The default is `auto_allocated_network` if not specified in the network variable.

`openstack network auto allocated topology create --or-show`

## Installation

To you this terraform module within Cacao, you simply need to import this module in the Templates section of Cacao. If you don't see the menu item for Templates menu, then you'll need to enable it in the Advance Users section, which is not available yet :).

You can also use this template directly by simply using the terraform cli. You will want to pair the terraform execution with the companion ansible, which is referenced in the `metadata.json`.

Note: don't use `default` authentication

## TODO
* input variable validation using submodule

## Contributing
Contributions are always welcome

## Authors and acknowledgment
This module was created initially by Edwin Skidmore, and maintained by the Cloud Native Services team at [CyVerse](https://www.cyverse.org). Please support our project.