# This is a general purpose terraform -> ansible inventory template
# Using this template, there are many ways to refer to hosts
#   'all' can be used to refer to all hosts, of course
#   'group_name[0]'
#   name of resource + index
# The k3-* groups should be left in for ansible-k3s, if we want to be able to use it
# if you want to change the template on an existing `terraform apply` or change the inputs, just do another `terraform apply` and the new inventory will be generated
all:
    hosts:
        ${ master_name}:
            ansible_host: ${ jupyterhub_hostname }
%{ for index, group in worker_ips ~}
        ${ worker_names[index] }:
            ansible_host: ${ worker_ips[index] }
%{ endfor ~}
    vars:
        CONNECTION_USERNAMES:
          - '${ cacao_user }'
          - 'ubuntu'
          - 'almalinux'
          - 'rocky'
          - 'centos'
#          - 'debian'
#          - 'fedora'
#          - 'cloud-user'
#          - 'opensuse'
          - 'root'
        CACAO_USER: ${ cacao_user }
        JH_DEPLOY_STRATEGY: "${ jupyterhub_deploy_strategy}"
        JH_PREPULL_IMAGES: ${ do_jh_precache_images }
%{ if do_share_enable ~}
        MANILA_SHARE_ENABLE: true
        MANILA_SHARE_SIZE: ${ share_size }
        MANILA_SHARE_ACCESS_KEY: ${ share_access_key }
        MANILA_SHARE_ACCESS_TO: ${ share_access_to }
        MANILA_SHARE_EXPORT_PATH: ${ share_export_path }
        MANILA_SHARE_ACCESS_MODE: ${ share_readonly ? "ReadOnlyMany" : "ReadWriteMany" }
        MANILA_MOUNT_PATH: "/mnt/${ share_access_to }"
        MANILA_CEPH_MONITORS: "${ share_ceph_monitors }"
        MANILA_CEPH_ROOT_PATH: "${ share_ceph_root_path }"
        MANILA_CEPH_CSI_CHART_VERSION: "3.6.2"
        JH_SHARED_STORAGE_ENABLE: true
        JH_SHARED_STORAGE_PV_NAME: ${ share_access_to }-pv
        JH_SHARED_STORAGE_PVC_NAME: ${ share_access_to }-pvc
        JH_SHARED_STORAGE_MOUNT_DIR: ${ jupyterhub_share_mount_dir }
%{ endif ~}
        JH_SINGLEUSER_IMAGE: ${ jupyterhub_singleuser_image }
        JH_SINGLEUSER_IMAGE_TAG: ${ jupyterhub_singleuser_image_tag }
        K3S_DOCKER_ENABLE: true
        # K3S_CALICO_ENABLE: true
        # K3s_FLANNEL_BACKEND: "host-gw"
        K3S_CLUSTER_CIDR: "192.168.0.0/16"
        K3S_VERSION: "v1.24.4+k3s1"
    children:
%{ if substr(jupyterhub_deploy_strategy, 0, 4) == "z2jh" ~}
        k3s_masters:
            hosts:
                ${ master_name }:
            vars:
                JH_CHART_VERSION: "2.0.0"
                # K3S_TRAEFIK_ENABLE: false
                # K3S_INGRESS_NGINX_ENABLE: true
                JH_INGRESS_ENABLED: true
                JH_SINGLEUSER_EXCLUDE_MASTER: ${ do_jh_singleuser_exclude_master }
%{ if jupyterhub_deploy_strategy == "z2jh_kubeadm" ~}
                JH_DB_PVC_STORAGE_CLASS_NAME: "local-path"
%{ endif ~}
%{ if jupyterhub_hostname_isset ~}
                JH_INGRESS_HOSTNAME: "${ jupyterhub_hostname }"
%{ endif ~}
                # this dictionary declares the jupyterhub master as well
                JH_AUTH_CLASS: ${ jupyterhub_authentication }
                JH_ALLOWED_USERS:
%{ for user in jupyterhub_allowed_users ~}
                    - ${ user }
%{ endfor ~}
                JH_ADMINS:
%{ for user in jupyterhub_admins ~}
                    - ${ user }
%{ endfor ~}
                JH_DUMMY_PASS: ${ jupyterhub_dummy_password }
                JH_OAUTH2_CLIENT_ID: ${ jupyterhub_oauth2_clientid }
                JH_OAUTH2_CLIENT_SECRET: ${ jupyterhub_oauth2_secret }
%{ if jupyterhub_oauth2_callback_https_enable ~}
                JH_OAUTH2_CALLBACK_URL: https://${ jupyterhub_hostname }/hub/oauth_callback
%{ else ~}
                JH_OAUTH2_CALLBACK_URL: http://${ jupyterhub_hostname }/hub/oauth_callback
%{ endif ~}
                JH_SINGLEUSER_DEFAULT_URL: ${ jupyterhub_singleuser_default_url }
%{ if jupyterhub_resource_request_cpu != null ~}
                JH_RESOURCES_REQUEST_CPU: ${ jupyterhub_resource_request_cpu }
%{ endif ~}
%{ if jupyterhub_resource_request_memory != null ~}
                JH_RESOURCES_REQUEST_MEMORY: ${ jupyterhub_resource_request_memory }
%{ endif ~}
%{ if jh_singleuser_cpu_min != null ~}
                JH_SINGLEUSER_CPU_GUARANTEE: ${ jh_singleuser_cpu_min }
%{ endif ~}
%{ if jh_singleuser_memory_min != null ~}
                JH_SINGLEUSER_MEMORY_GUARANTEE: "${ jh_singleuser_memory_min }G"
%{ endif ~}
%{ if jh_singleuser_cpu_max != null ~}
                JH_SINGLEUSER_CPU_LIMIT: ${ jh_singleuser_cpu_max }
%{ endif ~}
%{ if jh_singleuser_memory_max != null ~}
                JH_SINGLEUSER_MEMORY_LIMIT: "${ jh_singleuser_memory_max }G"
%{ endif ~}
        k3s_agents:
            hosts:
%{ for index, group in worker_names ~}
                ${ worker_names[index] }:
%{ endfor ~}
        k3s_cluster:
            children:
                k3s_masters:
                k3s_agents:
            vars:
%{ if jupyterhub_do_enable_gpu ~}
                JH_SINGLEUSER_GPU_ENABLE: true
                K3S_GPU_ENABLE: true

%{ endif ~}
%{ endif ~}
